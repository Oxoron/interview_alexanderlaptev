# What's this?

That's an interview task.

Implemented for: Danske Bank, GSL. 

Implemented by: Alexander Laptev.

Reviewer: gses@danskebank.lt

The task itself can be found at Task/Task.pdf

Solution tested on: Ubuntu 18.04

# Requirements
.NET Core 2.2 

# How to Build?
From the repo root use the command **:**
dotnet build PyramidWay/PyramidWay.sln

# How to Run?
From the repo root use the command **:**
dotnet PyramidWay/PyramidWay/bin/Debug/netcoreapp2.2/PyramidWay.dll "**fullPathToInputFile**/input.txt"

Alternative: 
* navigate to PyramidWay/PyramidWay/bin/Debug/netcoreapp2.2 
* put the *input.txt* near the PyramidWay.dll 
* execute the command **:** dotnet PyramidWay.dll

# How to Test?
From the repo root use the command **:** dotnet test PyramidWay/PyramidWayTests/PyramidWayTests.csproj

# Solution comments
**T**hat's not an optimal solution. It requires O(n²) of memory to copy the pyramid, and O(n²) operations to check all ways. 

If we don't care about the way itself (we are interested only in the sum), we can improve the result till the O(n) of memory and O(n²) of operations. Though the complexity is the same, the coplexity coefficient will be smaller, and the computation will be faster.

**A** workflow of the app is pretty simple: we create a builder, it parses the file and generates a pyramid in memory. Then we find the legal first way in the pypamid (ideally it's a way when we always go by the left child). After that we look for the next legal ways, and check all sums. In fact, there's a quazy-enumerator implemented over all ways, so it's possible to run it and compute the max way sum.

# What can be done more?
1. Use Release instead of Debug -- if there's a deploy process.
2. Use DI in the tests. Now it's required in the only test, so tests DI is delayed.
3. Add arguments validation and Exceptions handling. Skipped - not enough time. 
4. Add logging. Though there's no need in logging in this specific app.