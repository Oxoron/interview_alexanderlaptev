using System;
using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidLayerTests
    {
        [Test]
        public void Ctor_FirstLayer_ValidScenario()
        {
            // Arrange
            
            // Act
            PyramidLayer layer = new PyramidLayer(1,new []{1});
            
            // Assert
            Assert.AreEqual(1,layer[1].Value, "Error on the first node value");
            Assert.AreEqual(1,layer[1].LayerIndex, "Error on the first node layer index");
            Assert.AreEqual(1,layer[1].NodeIndex, "Error on the first node node index");
        }
        
        [Test]
        public void Ctor_NotTheFirst_ValidScenario()
        {
            // Arrange
            
            // Act
            PyramidLayer layer = new PyramidLayer(3,new []{1,3,2});
            
            // Assert
            Assert.AreEqual(1,layer[1].Value, "Error on the first node value");
            Assert.AreEqual(3,layer[1].LayerIndex, "Error on the first node layer index");
            Assert.AreEqual(1,layer[1].NodeIndex, "Error on the first node node index");
            Assert.AreEqual(2,layer[3].Value, "Error on the last node value");
            Assert.AreEqual(3,layer[3].LayerIndex, "Error on the last node layer index");
            Assert.AreEqual(3,layer[3].NodeIndex, "Error on the last node node index");
        }
        
        [Test]
        public void Indexed_ZeroElement_ExceptionExpected()
        {
            // Arrange
            PyramidLayer layer = new PyramidLayer(3,new []{1,3,2});
            
            Func<PyramidNode> layerIndexerCall = () => layer[0];
            TestDelegate @delegate = () => layerIndexerCall();
            
            // Act
            Assert.Throws<IndexOutOfRangeException>(@delegate);
        }
        
        [Test]
        public void Ctor_LayerAndValuesSizeMismatch_ExpectedException ()
        {
            // Arrange
            Func<PyramidLayer> constructLayer = () => new PyramidLayer(4,new []{1,3,2});
            TestDelegate @delegate = () => constructLayer();
            
            // Act
            Assert.Throws<ArgumentException>(@delegate);
        }
    }
}