using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidBuilderTests
    {
        [Test]
        public void BuildPyramid_ValidScenario()
        {
            // Arrange
            var builder = new PyramidBuilder(new PyramidLineParser(' '));
            var lines = new string[]
            {
                "1",
                "5 7",
                "-4 0 -3"
            };
            
            // Act
            Pyramid pyramid = builder.BuildPyramid(lines);
            
            // Assert
            Assert.AreEqual(3, pyramid.LayersCount);
            
            Assert.AreEqual(1,pyramid[1,1].Value);
            
            Assert.AreEqual(5,pyramid[2,1].Value);
            Assert.AreEqual(7,pyramid[2,2].Value);
            
            Assert.AreEqual(-4,pyramid[3,1].Value);
            Assert.AreEqual(0,pyramid[3,2].Value);
            Assert.AreEqual(-3,pyramid[3,3].Value);
        }
    }
}