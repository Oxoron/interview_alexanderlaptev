using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidWayTests
    {
        [Test]
        public void Sum()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {4},
                new[] {5, -3}
            });
            
            // Act
            var way = new PyramidWay.PyramidWay(new []{pyramid[1,1], pyramid[2,2]}, pyramid);
            

            // Assert
            Assert.AreEqual(1, way.Sum);
        }
    }
}