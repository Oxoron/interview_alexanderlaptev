using System;
using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidTests
    {
        [Test]
        public void Ctor_OneLayer_ValidScenario()
        {
            // Arrange
            
            // Act
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4}
            });
            
            // Assert
            Assert.AreEqual(1, pyramid.LayersCount);
            Assert.AreEqual(4,pyramid[1,1].Value);
        }
        
        [Test]
        public void Ctor_FewLayers_ValidScenario()
        {
            // Arrange
            
            // Act
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3}
            });
            
            // Assert
            Assert.AreEqual(2, pyramid.LayersCount);
            Assert.AreEqual(4,pyramid[1,1].Value);
            Assert.AreEqual(5,pyramid[2,1].Value);
            Assert.AreEqual(-3,pyramid[2,2].Value);
        }
        
        [Test]
        public void Indexed_ZeroElement_ExceptionExpected()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3}
            });
            
            Func<PyramidNode> pyramidIndexerCall = () => pyramid[0,1];
            TestDelegate @delegate = () => pyramidIndexerCall();
            
            // Act
            Assert.Throws<IndexOutOfRangeException>(@delegate);
        }
    }
}