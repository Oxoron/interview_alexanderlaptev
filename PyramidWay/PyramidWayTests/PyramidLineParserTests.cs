using System.Linq;
using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidLineParserTests
    {
        [Test]
        public void BaseParseTest()
        {
            // Arrange
            var parser = new PyramidLineParser(' ');
            
            // Act
            var result = parser.ParseLine(" 12 -1 4 ").ToArray();

            // Assert
            Assert.AreEqual(12, result[0]);
            Assert.AreEqual(-1, result[1]);
            Assert.AreEqual(4, result[2]);
        }
        
        [Test]
        public void FewWhitespaces_BetweenElements()
        {
            // Arrange
            var parser = new PyramidLineParser(' ');
            
            // Act
            var result = parser.ParseLine(" 12 -1  4 ").ToArray();

            // Assert
            Assert.AreEqual(12, result[0]);
            Assert.AreEqual(-1, result[1]);
            Assert.AreEqual(4, result[2]);
        }
    }
}