using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidWaySeekerTests
    {
        [Test]
        public void FindBestWay_OneNodePyramid()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {4}
            });
            PyramidWaySeeker seeker = new PyramidWaySeeker();
            
            // Act
            var maxWay = seeker.FindBestWay(pyramid);

            // Assert
            Assert.AreEqual(1, maxWay.Nodes.Length);
            Assert.AreEqual(4, maxWay.Nodes[0].Value);
            Assert.AreEqual(4, maxWay.Sum);
        }
        
        
        [Test]
        public void FindBestWay_LargestChildSelection()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {4},
                new[] {-3, 5}
            });
            PyramidWaySeeker seeker = new PyramidWaySeeker();
            
            // Act
            var maxWay = seeker.FindBestWay(pyramid);

            // Assert
            Assert.AreEqual(2, maxWay.Nodes.Length);
            Assert.AreEqual(4, maxWay.Nodes[0].Value);
            Assert.AreEqual(5, maxWay.Nodes[1].Value);
            Assert.AreEqual(9, maxWay.Sum);
        }
        
        
        [Test]
        public void FindBestWay_IgnoreSameOddChildren()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {4},
                new[] {3, 8}
            });
            PyramidWaySeeker seeker = new PyramidWaySeeker();
            
            // Act
            var maxWay = seeker.FindBestWay(pyramid);

            // Assert
            Assert.AreEqual(2, maxWay.Nodes.Length);
            Assert.AreEqual(4, maxWay.Nodes[0].Value);
            Assert.AreEqual(3, maxWay.Nodes[1].Value);
            Assert.AreEqual(7, maxWay.Sum);
        }
        
        [Test]
        public void FindBestWay_IgnoreDeadEnds()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {4},
                new[] {3, 7},
                new[] {3, 5, 8},
                
            });
            PyramidWaySeeker seeker = new PyramidWaySeeker();
            
            // Act
            var maxWay = seeker.FindBestWay(pyramid);

            // Assert
            Assert.AreEqual(3, maxWay.Nodes.Length);
            Assert.AreEqual(4, maxWay.Nodes[0].Value);
            Assert.AreEqual(7, maxWay.Nodes[1].Value);
            Assert.AreEqual(8, maxWay.Nodes[2].Value);
            Assert.AreEqual(19, maxWay.Sum);
        }
        
        
        [Test(Description = "The sample provided by the 'client'")]
        public void FindBestWay_TaskSample()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new[]
            {
                new[] {1},
                new[] {8, 9},
                new[] {1, 5, 9},
                new[] {4, 5, 2, 3}
                
            });
            PyramidWaySeeker seeker = new PyramidWaySeeker();
            
            // Act
            var maxWay = seeker.FindBestWay(pyramid);

            // Assert
            Assert.AreEqual(4, maxWay.Nodes.Length);
            Assert.AreEqual(1, maxWay.Nodes[0].Value);
            Assert.AreEqual(8, maxWay.Nodes[1].Value);
            Assert.AreEqual(5, maxWay.Nodes[2].Value);
            Assert.AreEqual(2, maxWay.Nodes[3].Value);
            Assert.AreEqual(16, maxWay.Sum);
        }
    }
}