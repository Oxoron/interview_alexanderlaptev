using NUnit.Framework;
using PyramidWay;

namespace PyramidWayTests
{
    public class PyramidWayExtensionsTests
    {
        #region CanContinueWith
        [TestCase(1,1,ExpectedResult = false)]
        [TestCase(2,1,ExpectedResult = false)]
        [TestCase(3,1,ExpectedResult = true)]
        public bool CanContinueWith_Layers(int layerIndex, int nodeIndex)
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3},
                new [] {2,4,6}
            });
            PyramidNode nodeUnderTest = pyramid[2, 1];

            // Act
            var result = nodeUnderTest.CanContinueWith(pyramid[layerIndex, nodeIndex]);

            // Assert - defined in the TestCase
            return result; 
        }
        
        [TestCase(4,1,ExpectedResult = false)]
        [TestCase(4,2,ExpectedResult = true)]
        [TestCase(4,3,ExpectedResult = true)]
        [TestCase(4,4,ExpectedResult = false)]
        public bool CanContinueWith_NodeIndex(int layerIndex, int nodeIndex)
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3},
                new [] {2,4,6},
                new []{3,5,7,9}
            });
            PyramidNode nodeUnderTest = pyramid[3, 2];

            // Act
            var result = nodeUnderTest.CanContinueWith(pyramid[layerIndex, nodeIndex]);

            // Assert - defined in the TestCase
            return result; 
        }
        
        [TestCase(2,1,3,1, ExpectedResult = false)]
        [TestCase(2,1,3,2, ExpectedResult = true)] 
        [TestCase(2,2,3,2, ExpectedResult = false)] 
        [TestCase(2,2,3,3, ExpectedResult = true)] 
        public bool CanContinueWith_OddEven(int parentLayer, int parentNode, int childLayer, int chileNode)
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{1,2},
                new [] {1,2,3}
            });
            var parent = pyramid[parentLayer, parentNode];
            var childUnderTest = pyramid[childLayer, chileNode];
            

            // Act
            var result = parent.CanContinueWith(childUnderTest);

            // Assert - defined in the TestCase
            return result; 
        }
        #endregion
        
        [Test]
        public void OrderedProbableContinuations_BaseTest()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3},
                new [] {2,4,6}
            });
            PyramidNode nodeUnderTest = pyramid[2, 1];

            // Act
            var probableContinuations = nodeUnderTest.OrderedProbableContinuations(pyramid);

            // Assert 
            Assert.AreEqual(2, probableContinuations.Length); 
            
            Assert.AreEqual(3, probableContinuations[0].LayerIndex);
            Assert.AreEqual(1, probableContinuations[0].NodeIndex);
            
            Assert.AreEqual(3, probableContinuations[1].LayerIndex);
            Assert.AreEqual(2, probableContinuations[1].NodeIndex);
        }
        
        [Test]
        public void OrderedProbableContinuations_LastLayerTest()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3},
                new [] {2,4,6}
            });
            PyramidNode nodeUnderTest = pyramid[3, 1];

            // Act
            var probableContinuations = nodeUnderTest.OrderedProbableContinuations(pyramid);

            // Assert 
            Assert.AreEqual(0, probableContinuations.Length);
        }
        

        #region FirstWay
        [Test]
        public void FirstWay_SmallPyramid()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{1}
            });
            
            // Act
            var way = pyramid.FirstWay();

            // Assert 
            Assert.AreEqual(1, way.Nodes.Length);
            Assert.AreEqual(1, way.Nodes[0].Value);
        }
        
        [Test]
        public void FirstWay_BaseTest()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{1},
                new []{2,4},
                new [] {3,5,7}
            });
            
            // Act
            var way = pyramid.FirstWay();

            // Assert 
            Assert.AreEqual(3, way.Nodes.Length);
            Assert.AreEqual(1, way.Nodes[0].Value);
            Assert.AreEqual(2, way.Nodes[1].Value);
            Assert.AreEqual(3, way.Nodes[2].Value);
        }
        
        [Test]
        public void FirstWay_WithBlockerContinuations()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{1},
                new []{3,4},
                new [] {5,7,6}
            });
            
            // Act
            var way = pyramid.FirstWay();

            // Assert 
            Assert.AreEqual(3, way.Nodes.Length);
            Assert.AreEqual(1, way.Nodes[0].Value);
            Assert.AreEqual(4, way.Nodes[1].Value);
            Assert.AreEqual(7, way.Nodes[2].Value);
        }
        
        [Test]
        public void FirstWay_WithDeadEnd()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{1},
                new []{2,4},
                new [] {8,6,7}
            });
            
            // Act
            var way = pyramid.FirstWay();

            // Assert 
            Assert.AreEqual(3, way.Nodes.Length);
            Assert.AreEqual(1, way.Nodes[0].Value);
            Assert.AreEqual(4, way.Nodes[1].Value);
            Assert.AreEqual(7, way.Nodes[2].Value);
        }
        #endregion 
        
        [Test]
        public void NextWay_BaseTest()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{5,-3},
                new [] {2,5,6}
            });
            var firstWay = pyramid.FirstWay();

            // Act
            var nextWay = firstWay.NextWay();

            // Assert 
            Assert.AreEqual(3, nextWay.Nodes.Length);
            Assert.AreEqual(4, nextWay.Nodes[0].Value);
            Assert.AreEqual(-3, nextWay.Nodes[1].Value);
            Assert.AreEqual(6, nextWay.Nodes[2].Value);
        }
        
        [Test]
        public void NextWay_ForLastWay_NullExpected()
        {
            // Arrange
            Pyramid pyramid = new Pyramid(new []
            {
                new []{4},
                new []{2,-3},
                new [] {2,5,6}
            });
            var lastWay = pyramid.FirstWay(); // The first is the last for this pyramid

            // Act
            var nextWay = lastWay.NextWay();

            // Assert 
            Assert.IsNull(nextWay);
        }
    }
}