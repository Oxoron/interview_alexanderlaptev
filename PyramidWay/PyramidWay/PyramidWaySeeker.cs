using PyramidWay.Interfaces;

namespace PyramidWay
{
    public class PyramidWaySeeker : IPyramidWaySeeker
    {
        public PyramidWay FindBestWay(Pyramid pyramid)
        {
            // Look for the first way
            var way = pyramid.FirstWay();
            
            // Iterate over all possible ways, looking for the one with the max sum
            var maxWay = way;
            do
            {
                way = way.NextWay();
                if (null == way) continue;
                if (way.Sum > maxWay.Sum){maxWay = way;}
            } while (way != null);

            return maxWay;
        }
    }
}