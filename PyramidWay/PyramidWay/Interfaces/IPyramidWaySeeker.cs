namespace PyramidWay.Interfaces
{
    public interface IPyramidWaySeeker
    {
        PyramidWay FindBestWay(Pyramid pyramid);
    }
}