using System.Collections.Generic;

namespace PyramidWay.Interfaces
{
    public interface IPyramidLineParser
    {
        IEnumerable<int> ParseLine(string line);
    }
}