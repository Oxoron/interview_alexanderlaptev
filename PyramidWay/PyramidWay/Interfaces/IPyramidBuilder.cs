using System.Collections.Generic;

namespace PyramidWay.Interfaces
{
    public interface IPyramidBuilder
    {
        Pyramid BuildPyramid(IEnumerable<string> inputLines);
    }
}