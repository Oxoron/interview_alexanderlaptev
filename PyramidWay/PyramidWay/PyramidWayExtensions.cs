using System;
using System.Collections.Generic;
using System.Linq;

namespace PyramidWay
{
    public static class PyramidWayExtensions
    {
        public static PyramidWay NextWay(this PyramidWay way)
        {
            PyramidNode[] nodes = new PyramidNode[way.Pyramid.LayersCount + 1];
            Array.Copy(way.Nodes,0,nodes,1,way.Pyramid.LayersCount);
            
            var currentLayer = way.Pyramid.LayersCount - 1;
            var currentNode = nodes[currentLayer];
            
            nodes = ComputeNextWayNodes(nodes, currentLayer, currentNode, way.Pyramid); 
            
            if(nodes == null){return null;}
            return new PyramidWay(
                nodes.Where(node => node != null).OrderBy(node => node.LayerIndex).ToArray(),
                way.Pyramid);
        }

        
        // Be careful: this method mutates args
        // nodes layers in the nodes argument match the element index in the array
        private static PyramidNode[] ComputeNextWayNodes(PyramidNode[] nodes, int currentLayer, PyramidNode currentNode, Pyramid pyramid)
        {
            while (currentLayer != pyramid.LayersCount && currentLayer != 0)
            {
                var continuation = currentNode.GetFirstValidChildAfter(nodes[1 + currentLayer], pyramid);

                if (continuation != null) // If haven't check any child yet - let's check it
                {
                    nodes[1 + currentLayer] = continuation;
                    currentNode = continuation;
                    currentLayer++;
                }
                else // If all children have been check, but we still didn't fine the way -- we are in the dead end
                {
                    nodes[1 + currentLayer] = null; // So, we have to step one layer up
                    currentLayer--;
                    if (currentLayer != 0)
                    {
                        currentNode = nodes[currentLayer];
                    }
                }
            }

            if(currentLayer == 0){return null;}
            return nodes;
        }

        private static PyramidNode GetFirstValidChildAfter(this PyramidNode node, 
            PyramidNode lastCheckedChild, Pyramid pyramid)
        {
            IEnumerable<PyramidNode> probableContinuations = node
                .OrderedProbableContinuations(pyramid)
                .Where(child => node.CanContinueWith(child));
            
            if (lastCheckedChild != null) // If we already tried at least on of the children of the current node
            {
                probableContinuations = probableContinuations // We filter out these children
                    .Where(child => child.IsOnAWayAfter(lastCheckedChild));  // Which has been visited already
            }
                
            var result = probableContinuations.FirstOrDefault();
            return result;
        }
        private static bool IsOnAWayAfter(this PyramidNode arg1, PyramidNode arg2)
        {
            if (arg1.LayerIndex != arg2.LayerIndex)
            {
                throw new ArgumentException("Can't compare nodes positions: they are on different layers'");
                
            }

            return arg1.NodeIndex > arg2.NodeIndex;
        }
        
        public static PyramidWay FirstWay(this Pyramid pyramid)
        {
            PyramidNode[] nodes = new PyramidNode[1+pyramid.LayersCount]; // Layers numeration starts from 1. Don't use the zero element of the array
            
            int currentLayer = 1;
            var currentNode = pyramid[1, 1];
            nodes[currentLayer] = currentNode;
            
            nodes = ComputeNextWayNodes(nodes, currentLayer, currentNode, pyramid);

            return new PyramidWay(
                nodes.Where(node => node != null).OrderBy(node => node.LayerIndex).ToArray(),
                pyramid);
        }

        public static PyramidNode[] OrderedProbableContinuations(this PyramidNode node, Pyramid pyramid)
        {
            if(node.LayerIndex == pyramid.LayersCount) return new PyramidNode[0];
            return new[]
            {
                pyramid[node.LayerIndex+1,node.NodeIndex],
                pyramid[node.LayerIndex+1,node.NodeIndex+1]
            };
        }

        public static bool CanContinueWith(this PyramidNode node, PyramidNode continuation)
        {
            if (continuation.LayerIndex != 1 + node.LayerIndex)
            {
                return false;
            }

            if (continuation.NodeIndex != node.NodeIndex
                && continuation.NodeIndex  != node.NodeIndex + 1)
            {
                return false;
            }

            if ((node.Value + continuation.Value) % 2 == 0) // That's and odd-even change clause
            {
                return false;
            }

            return true;
        }
    }
}