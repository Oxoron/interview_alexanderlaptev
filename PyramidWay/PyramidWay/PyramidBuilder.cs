using System.Collections.Generic;
using System.Linq;
using PyramidWay.Interfaces;

namespace PyramidWay
{
    public class PyramidBuilder : IPyramidBuilder
    {
        private readonly IPyramidLineParser _lineParser;
        public PyramidBuilder(IPyramidLineParser lineParser)
        {
            this._lineParser = lineParser;
        }

        public Pyramid BuildPyramid(IEnumerable<string> inputLines)
        {
            var parsedLines = inputLines
                .Select((line, index) => _lineParser.ParseLine(line));
            
            return new Pyramid(parsedLines);
        }
    }
}