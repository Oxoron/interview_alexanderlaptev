using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PyramidWay
{
    public class PyramidLayer : IEnumerable<PyramidNode>
    {
        private readonly PyramidNode[] _nodes;
        public PyramidNode this[int index] => _nodes[index-1];
        public PyramidLayer(int layerIndex, IEnumerable<int> values)
        {
            if(values == null)
            {
                throw  new ArgumentNullException(nameof(values), 
                    "Can't build a pyramid layer: values array is null'");
            }
            
            var nodes = values
                .Select((value, index) => new PyramidNode(layerIndex, 1+index, value))
                .ToArray();
            
            if (nodes.Length != layerIndex)
            {
                string error = $"An error occured on the creation of the layer {layerIndex}: " +
                               $"There are {nodes.Length} values provided.{Constants.NewLine}" +
                               $"Every layer should have the same index with an amount of its elements.";
                throw new ArgumentException(error);
            }

            _nodes = nodes;
        }

        public IEnumerator<PyramidNode> GetEnumerator()
        {
            return ((IEnumerable<PyramidNode>) _nodes).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}