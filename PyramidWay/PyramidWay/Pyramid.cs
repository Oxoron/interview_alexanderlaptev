using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PyramidWay
{
    public class Pyramid : IEnumerable<PyramidLayer>
    {
        private readonly PyramidLayer[] _layers;
        public PyramidNode this[int layerIndex, int nodeIndex] => _layers[layerIndex-1][nodeIndex];
        public int LayersCount => _layers.Length;
        public Pyramid(IEnumerable<IEnumerable<int>> values)// For small pyramids it's worth to use int[][], but for large IEnumerable maybe better
        {
            if(values == null)
            {
                throw  new ArgumentNullException(nameof(values), 
                    "Can't build a pyramid: layers array is null'");
            }

            _layers = values
                .Select((layer, index) => new PyramidLayer(1+index, layer))
                .ToArray();
        }

        public IEnumerator<PyramidLayer> GetEnumerator()
        {
            foreach (var layer in _layers)
            {
                yield return layer;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}