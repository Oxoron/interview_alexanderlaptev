﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using PyramidWay.Interfaces;

namespace PyramidWay
{
    // Parse arguments in order to retrieve the input file full path
    // For complex arguments it's worth to use CommandLineParser (https://github.com/commandlineparser/commandline)
    // In our case it's ok to stop on the single method
    internal class ArgumentsParser
    {
        private const string DefaultInputFileName = "input.txt";
        
        private const int InputFileNotFoundReturnCode = 17;
        private const int DefaultInputFileNotFoundReturnCode = 18;
        
        internal string GetInputFileFullPath(string[] args)
        {
            if (args != null && args.Length > 0) // Validate argument passed to the program
            {
                if (File.Exists(args[0]))
                {
                    return args[0];
                }
                else
                {
                    Console.WriteLine($"Can't find the input file {args[0]} ." +
                                      $"The program will be terminated.");
                    Environment.Exit(InputFileNotFoundReturnCode);
                }
            }
            else // If there's not any argument passed to the program - validate a default file
            {
                if (File.Exists(DefaultInputFileName))
                {
                    return DefaultInputFileName;
                }
                else
                {
                    Console.WriteLine($"Can't fine the default input file {DefaultInputFileName}' ." +
                                      $"The program will be terminated.");
                    Environment.Exit(DefaultInputFileNotFoundReturnCode);
                }
            }
            
            throw new InvalidOperationException("Unexpected error happened on the arguments parse. " +
                                                "The first argument should contain the full path to the input file.'");
        }
    }
    
    // Both ArgumentParser and ResultsFormatter relate to the infrastructure
    // Both of them are internal, both of the them are ignored in tests and DI
    internal class ResultsFormatter
    {
        internal void PrintResult(PyramidWay maxWay)
        {
            Console.WriteLine($"Max sum: {maxWay.Sum}");
            Console.WriteLine($"Path: {FormatWayOutput(maxWay)}");
        }
        
        private string FormatWayOutput(PyramidWay way)
        {
            return String.Join(", ", way.Nodes.Select(node => node.Value));
        }
    }
    
    class Program
    {
        private static ServiceProvider SetUpServiceProvider()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IPyramidBuilder, PyramidBuilder>()
                .AddSingleton<IPyramidLineParser>(new PyramidLineParser(' '))
                .AddSingleton<IPyramidWaySeeker, PyramidWaySeeker>()
                .BuildServiceProvider();

            return serviceProvider;
        }
        static void Main(string[] args)
        {
            // Set up DI
            var serviceProvider = SetUpServiceProvider();

            // Parse arguments, get the input file name
            string inputFile = new ArgumentsParser().GetInputFileFullPath(args);
            
            // Copy the pyramid to the memory 
            var inputLines = File.ReadLines(inputFile);
            var pyramid = serviceProvider.GetService<IPyramidBuilder>()
                .BuildPyramid(inputLines);

            // Look for the best way (see the task for description)
            var maxWay = serviceProvider.GetService<IPyramidWaySeeker>()
                .FindBestWay(pyramid);
            
            // Output the result
            new ResultsFormatter().PrintResult(maxWay);
        }
    }
}