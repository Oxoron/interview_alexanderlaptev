using System.Linq;

namespace PyramidWay
{
    public class PyramidWay
    {
        public PyramidNode[] Nodes { get; private set; }
        public Pyramid Pyramid { get; private set; }
        public int Sum => Nodes.Sum(node => node.Value);

        public PyramidWay(PyramidNode[] nodes, Pyramid pyramid)
        {
            Nodes = nodes;
            Pyramid = pyramid;
        }
    }
}