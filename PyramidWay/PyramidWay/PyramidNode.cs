namespace PyramidWay
{
    public class PyramidNode
    {
        public readonly int LayerIndex;
        public readonly int NodeIndex;
        public readonly int Value;

        public PyramidNode(int layerIndex, int nodeIndex, int value)
        {
            LayerIndex = layerIndex;
            NodeIndex = nodeIndex;
            Value = value;
        }

        public override string ToString()
        {
            return $"{LayerIndex},{NodeIndex},{Value}";
        }
    }
}