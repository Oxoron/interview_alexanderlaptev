using System;
using System.Collections.Generic;
using PyramidWay.Interfaces;

namespace PyramidWay
{
    public class PyramidLineParser : IPyramidLineParser
    {
        private readonly char _separator;

        public PyramidLineParser(char separator)
        {
            _separator = separator;
        }

        public IEnumerable<int> ParseLine(string line)
        {
            string[] splitLine = line.Trim().Split(_separator);
            foreach (var numberAsString in splitLine)
            {
                if(String.IsNullOrWhiteSpace(numberAsString)){continue;}
                yield return Int32.Parse(numberAsString);
            }
        }
    }
}